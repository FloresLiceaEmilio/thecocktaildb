// ` 
document.addEventListener("DOMContentLoaded", function(){
    const btnbuscar = document.getElementById('btnBusca');
    const inputAl = document.getElementById('inputAl');
    const inputNo = document.getElementById('inputNo');
    const btnlimpiar = document.getElementById('btnLimpiar');
    const listaBebidas = document.getElementById('grid');

    btnbuscar.addEventListener("click", function(){

        let coctel;

        if(inputAl.checked) {
           
            coctel = "Alcoholic";
        } else if(inputNo.checked) {
           
            coctel = "Non_Alcoholic";
        } else {
            // Ningún radio button está seleccionado
            console.error("No elegiste nada");
            return;
        }

            fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${coctel}`)
            .then(response => response.json())
            .then(data => {
                 // Limpiar la lista de bebidas antes de agregar nuevas
                 listaBebidas.innerHTML = '';

                 // Recorrer las bebidas y agregarlas a la lista
                 data.drinks.forEach(bebida => {
                    const nombreBebida = bebida.strDrink;
                    const imagenBebida = bebida.strDrinkThumb;
                
                    const listItem = document.createElement('li');
                    const bebidaIMG = document.createElement('img');
                
                    bebidaIMG.src = imagenBebida;
                    bebidaIMG.alt = nombreBebida;
                
                    listItem.appendChild(bebidaIMG);
                
                    const nombreElement = document.createElement('span');
                    nombreElement.appendChild(document.createTextNode(nombreBebida));
                    listItem.appendChild(nombreElement);
                
                    listaBebidas.appendChild(listItem);
                });
                
            })
            .catch(error => {
                console.error("Error en la solicitud:", error);
            });
        
            btnlimpiar.addEventListener("click", function(){
                // Limpiar la lista de bebidas al hacer clic en el botón "Limpiar"
                listaBebidas.innerHTML = '';
            }); 
    })

})